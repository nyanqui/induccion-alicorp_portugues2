var totalResults = 0;
var media  = [];
var videoExtension = ["mp4"];
var audioExtension = ["mp3"];

/**
 * [mediaContent Crea y carga los elementos de audio y Video en el DOM
 *               uno por uno ]
 * @param  {[Array]} media [Con el los audios]
 * @return {[type]}       [description]
 */
function mediaContent(media){

  var thisFn = arguments.callee;
  
  if( media && media.length > 0){
    
    var file    = media[0].split("/");
    var fileName = file[1];
  
  	var fileExtension = fileName.split(".");
  		fileExtension = fileExtension[1];
  	
    var typeElement = "audio";
    var className   = "audio-item";
    var contentType   = "audio/mpeg";
    if(videoExtension.indexOf(fileExtension) != -1){
        typeElement = "video";
        className   = "video-item";
        contentType = "video/mpeg";
    }
    
    var element = document.createElement(typeElement);
        if( typeElement == 'video'){
            element.width  = "0";
            element.height = "0";
        }
        element.src = media[0];
        element.id  = fileName;
        element.className = className;
        element.type = contentType;
        
        media.shift();

        /*loadeddata*/
        element.addEventListener("loadeddata",function(){

          if(element.readyState) 
           var rest  = (totalResults - media.length);
           var width = Math.round((rest * 100)/totalResults);
           
           updateAvance(width+"%")
           //call recursive
           thisFn(media);

        },false);
       
        jQuery("#reproductor").append(element);

  }else{
      //completed all media files
      jQuery("#modalLoading").modal("hide");
      var firstAudio  = document.getElementById("d02-01.mp3");
      var secondAudio = document.getElementById("d02-02.mp3");
      if(firstAudio && secondAudio){
	   
	      firstAudio.play();
	      firstAudio.onended = function(){
			     secondAudio.play();      	
	      }
      }
  }
}

function updateAvance(width){
  
	jQuery("#progress-bar","#modalLoading").css({width:width});
	jQuery("#avance","#modalLoading").html(width);          

    return true;
}

function loadResources(){
  
  jQuery("#modalLoading").modal("show");

  var data = ["audios/d02-01.mp3","audios/d02-02.mp3","audios/d41-incorrecto.mp3","audios/d41-correcto.mp3",
              "audios/nn50.mp3","audios/nn51.mp3",
              "audios/nn52.mp3","audios/nn53.mp3","audios/nn54.mp3",
              "audios/cv55.mp3","audios/cv56.mp3","audios/cv57.mp3","audios/cv58.mp3","audios/cv59.mp3","audios/cv60.mp3","audios/cv61.mp3","audios/cv62.mp3","audios/cv63.mp3","audios/cv64.mp3","audios/cv65.mp3",
              "audios/vc66.mp3","audios/vc66.mp3","audios/vc67.mp3","audios/vc68.mp3","audios/vc69.mp3","audios/vc70.mp3","audios/vc71.mp3","audios/vc72.mp3","audios/vc73.mp3","audios/vc74.mp3","audios/vc75.mp3","audios/vc76.mp3","audios/vc77.mp3","audios/vc78.mp3",
              "audios/ae01.mp3","audios/ae02.mp3","audios/ae03.mp3"];



  totalResults =  data.length;
  mediaContent(data);
	
}

function loadPage(url){     
    stopAll(); 
    jQuery("#container-curso").load(url,function(){
    jQuery("#header-content").show();
  });
}

function stopAll(){

  var elmentsAudio = document.getElementsByClassName("audio-item");
  var elmentsVideo = document.getElementsByClassName("video-item");    
   
  for (var i = 0; i < elmentsAudio.length; i++) {
       //if(elmentsAudio[i].currentTime > 0){
           elmentsAudio[i].pause();
           elmentsAudio[i].currentTime = 0;
       //}
  }
  
  for (var y = 0; y < elmentsVideo.length; y++) {
      //if(elmentsVideo[y].currentTime >0){
          elmentsVideo[y].pause();
          elmentsVideo[y].currentTime = 0;
      //}
   
  }


  return true;

}

jQuery(document).ready(function(){
 	  
    jQuery("#modalLoading").modal({
        keyboard:false,
        backdrop:'static'
    });

    jQuery("#container-curso").delegate("a:not(.notClicked)","click",function(event){
        event.preventDefault();
        event.stopPropagation();
      
        var href = jQuery(this).attr("href");
        
        loadPage(href);

    });
});